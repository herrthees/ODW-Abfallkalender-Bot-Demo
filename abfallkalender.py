import locale
from datetime import datetime, timedelta
from random import choice

import requests

# Morgiges Datum für API-Abfrage und der Name des Wochentags in Deutsch
# Sprache auf Deutsch setzen, für die Namen der Wochentage
locale.setlocale(locale.LC_ALL, 'de_DE.UTF-8')

# Einen Tag auf das heutige Datum addieren in der UTC-Zeitangabe
utcMorgen = datetime.today()+timedelta(days=1)

# Das UTC-Datum im Format für die Open-Data-API, z.B. 2022-07-13
morgen = utcMorgen.strftime('%Y-%m-%d')

# Der Wochentagname von morgen
morgenWochentag = utcMorgen.strftime('%A')

# Gimmick: Eine Liste mit Sätzen
# für die Einleitung der Abfallkalender-Ausgabe
introSaetze = [
    'Leute, denkt morgen an die Müllabfuhr: ',
    'Morgen ist ' + morgenWochentag+', für die Müllabfuhr in Würzburg heißt das: ',
    'Der Abfallkalender für morgen, ' + morgenWochentag+': ',
    'Wer ist am ' + morgenWochentag+' beim Müll mit was dran?'
]

# Variablen initialisieren
ausgabe = ''
stadtteil = {}
stadtteilListe = set

# Daten für morgen vom Open-Data-Portal holen und in JSON umwandeln
# Siehe https://opendata.wuerzburg.de/explore/dataset/abfallkalender-wuerzburg/information/
abfallkalenderApiUrl = 'https://opendata.wuerzburg.de/api/records/1.0/search/?dataset=abfallkalender-wuerzburg&q=start='+morgen+'&rows=99'
apiAntwort = requests.get(abfallkalenderApiUrl)
abfallkalender = apiAntwort.json()

# Zu Testzwecken: Gibt das Dictionary aus
print (abfallkalender)

# Gibt es Daten für den morgigen Tag (Samstag und Sonntag nicht)
if len(abfallkalender['records']) > 0:

    # Daten neu struktierien:
    # Dictionary mit Stadtteilname als Schlüssel und einer Liste der Abfallart,
    # die morgen dort geholt wird
    for orte in abfallkalender['records']:
        try:
            stadtteil[orte['fields']['stadtteil_name']].append(
                orte['fields']['kategorie'])
        except KeyError:
            stadtteil[orte['fields']['stadtteil_name']] = []
            stadtteil[orte['fields']['stadtteil_name']].append(
                orte['fields']['kategorie'])

    # Ausgabe zusammenbasteln:
    for (aktuellerStadtteil, abfallart) in stadtteil.items():
        ausgabe += aktuellerStadtteil+': '
        # mehr als eine Abfallart? Dann mit Kommans und Und verknüfpfen
        if len(abfallart) > 1:
            ausgabe += ', '.join(abfallart[:-1])+' und '+abfallart[-1]+"\n"
        # sonst die eine Abfallart anhängen
        else:
            ausgabe += abfallart[0]+"\n"

    # Zufällig einen Einleitungssatz auswählen und der Ausgabe voranstellen.
    ausgabe = choice(introSaetze)+'\n\n'+ausgabe

    # Die Ausgabe
    print(ausgabe)

# Wenn es keine Daten für den morgigen Tag gibt
else:
    print('Morgen keine Termine im Abfallkalender.')
